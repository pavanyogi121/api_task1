<?php

namespace CustomBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;

class DefaultController extends Controller
{
    /**
     * @Route("/")
     */
    public function indexAction()
    {
        return $this->render('CustomBundle:Default:index.html.twig');
    }

    /**
     * @Route("/hello-world")
     */
    public function showAction()
    {
        // return $this->render('CustomBundle:Default:index.html.twig');
        // return $this->render("<html><body><h1>Hello World</h1></body></html>");
        return new Response("<html><body><h1>Hello World</h1></body></html>");
    }
}
