<?php

namespace AppBundle\Entity;

use FOS\UserBundle\Model\User as BaseUser;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * @ORM\Table(name="fos_user")
 */
class User extends BaseUser
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    public function __construct()
    {
        parent::__construct();
        // your own logic
    }

    public function mailAction()
    {
        // returns the first mailer
        $container->get('swiftmailer.mailer.first_mailer');

        // also returns the second mailer since it is the default mailer
        $container->get('swiftmailer.mailer');

        // returns the second mailer
        $container->get('swiftmailer.mailer.second_mailer');
    }
}
