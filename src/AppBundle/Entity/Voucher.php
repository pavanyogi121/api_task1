<?php

namespace AppBundle\Entity;

use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Bridge\Doctrine\Validator\Constraints as DoctrineAssert;
use Doctrine\ORM\Mapping as ORM;

/**
 * Voucher
 *
 * @ORM\Table(name="voucher")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\VoucherRepository")
 * @DoctrineAssert\UniqueEntity(fields="serialnumber", message="serialnumber already exists")
 */
class Voucher
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
    * @ORM\ManyToOne(targetEntity="Operator", inversedBy="voucher")
    * @ORM\JoinColumn(name="operator_id", referencedColumnName="id")
    */
    private $operator;

    /**
     * @var string
     *
     * @ORM\Column(name="serialnumber", type="string", length=255, unique=true)
     */
    private $serialnumber;

    /**
     * @var string
     *
     * @ORM\Column(name="amount", type="decimal", precision=10, scale=0, nullable=true)
     */
    private $amount;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="expiry", type="date")
     */
    private $expiry;

    /**
     * @var bool
     *
     * @ORM\Column(name="state", type="boolean", nullable=true, options={"default" : false})
     */
    private $state = false;

    

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set serialnumber
     *
     * @param string $serialnumber
     *
     * @return Voucher
     */
    public function setSerialnumber($serialnumber)
    {
        $this->serialnumber = $serialnumber;

        return $this;
    }

    /**
     * Get serialnumber
     *
     * @return string
     */
    public function getSerialnumber()
    {
        return $this->serialnumber;
    }

    /**
     * Set amount
     *
     * @param string $amount
     *
     * @return Voucher
     */
    public function setAmount($amount)
    {
        $this->amount = $amount;

        return $this;
    }

    /**
     * Get amount
     *
     * @return string
     */
    public function getAmount()
    {
        return $this->amount;
    }

    /**
     * Set expiry
     *
     * @param \DateTime $expiry
     *
     * @return Voucher
     */
    public function setExpiry($expiry)
    {
        $this->expiry = $expiry;

        return $this;
    }

    /**
     * Get expiry
     *
     * @return \DateTime
     */
    public function getExpiry()
    {
        return $this->expiry;
    }

    /**
     * Set state
     *
     * @param boolean $state
     *
     * @return Voucher
     */
    public function setState($state)
    {
        $this->state = $state;

        return $this;
    }

    /**
     * Get state
     *
     * @return boolean
     */
    public function getState()
    {
        return $this->state;
    }

    /**
     * Set operator
     *
     * @param \AppBundle\Entity\Operator $operator
     *
     * @return Voucher
     */
    public function setOperator(\AppBundle\Entity\Operator $operator = null)
    {
        $this->operator = $operator;

        return $this;
    }

    /**
     * Get operator
     *
     * @return \AppBundle\Entity\Operator
     */
    public function getOperator()
    {
        return $this->operator;
    }
}
