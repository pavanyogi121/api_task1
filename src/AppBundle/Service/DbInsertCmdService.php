<?php

namespace AppBundle\Service;

use Doctrine\ORM\EntityManagerInterface;
use AppBundle\Entity\Voucher;
use AppBundle\Entity\Operator;

/**
 * DbInsertCmdService
 * class for database insertion operations
 */
class DbInsertCmdService
{
    protected $em;

    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->em = $entityManager;
    }

    /**
     * function insertVouchersCmd to insert voucher details into Voucher table
     */
    public function insertVouchersCmd($fileContent, $operator_id, $expiray_date)
    {
        $operatorRepo = $this->em->getRepository('AppBundle:Operator');
        $operatorData = $operatorRepo->find($operator_id);
        if (!$operatorData) {
            return ["id_not_found" => "id_not_found"];
        }
        $ar = [];
        foreach ($fileContent as $value) {
            $voucher = new Voucher();
            $voucher->setOperator($operatorData);
            $voucher->setExpiry(date_create($expiray_date));
            $voucher->setSerialnumber($value);
            $this->em->persist($voucher);
            try {
                $this->em->flush();
            } catch (\Doctrine\DBAL\DBALException $e) {
                return ["DBALException" => $e->getMessage()];
            }
           
            array_push($ar, [
            "status" => "0",
            "data" => [
                    "id" => $voucher->getId(),
                    "NetId" => $voucher->getOperator()->getNetworkId(),
                    "expiray" => $voucher->getExpiry(),
                    "Serial_No" => $voucher->getSerialnumber()
                    ]
            ]);
        }

        return $ar;
    }

    /**
     * function insertToDb to insert new operator details into database
     */
    public function insertToDb($params)
    {
        $this->em->getRepository('AppBundle:Operator');
        $operator = new Operator();
        
        $operator->setName($params["name"]);
        $operator->setNetworkId($params["netId"]);
        $this->em->persist($operator);
        try {
            $this->em->flush();
        } catch (\Doctrine\DBAL\DBALException $e) {
            return (["DBALException" => $e->getMessage()]);
        }

        $responseArray = [
                                "status" => "0",
                                "data" => [
                                "id" => $operator->getId(),
                                "name" => $operator->getName(),
                                "netId" => $operator->getNetworkId()
                                ]
                            ];

        return $responseArray;
    }
}
