<?php

namespace AppBundle\Service;

use Doctrine\ORM\EntityManagerInterface;

/**
 * InputValidationService
 * class for user input validations
 */

class InputValidationService
{
    /**
     * function inputValidationVoucher for netId and amount validation
     */
    public function inputValidationVoucher($params)
    {
        if (array_key_exists('netId', $params) && array_key_exists('amount', $params)) {
            if (!empty($params['netId']) && !empty($params['amount'])) {
                if (!is_numeric($params['netId']) || !is_numeric($params['amount'])) {
                    return false;
                }
                return true;
            } else {
                return false;
            }
        }
    }

    /**
     * function inputValidationOperator  for name, netId validation
     */
    public function inputValidationOperator($params)
    {
        if (array_key_exists('name', $params) && array_key_exists('netId', $params)) {
            if (!empty($params["name"]) && !empty($params["netId"])) {
                return true;
            } 
            return false;
            
        } else {
            return false;
        }
    }
}
