<?php

namespace AppBundle\Command;

use AppBundle\EventListener;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use \DateTime;

/**
 * CreateCustomeUsrCommand
 * class for custom user command to upload pin numbers to the database from csv file.
 * usage -   php bin/console  create_custome_usr "serial_numbers.csv" "1"  "2018-06-07"
 */
class CreateCustomeUsrCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $this
            ->setName('create_custome_usr')
            ->setDescription('Creates a new user.')
            ->addArgument('csv_filename', InputArgument::REQUIRED, 'The filename of the csv file ')
            ->addArgument('operator_id_name', InputArgument::REQUIRED, 'The operator_id_name ')
            ->addArgument('expiry_date_value', InputArgument::REQUIRED, 'The expiry_date_value ')
            ->addOption('option', null, InputOption::VALUE_NONE, 'Option description')
        ;
    }
    
    /**
     * validateDate
     * function to validate  input date
     */
    protected function validateDate($date, $format = 'Y-m-d')
    {
        $d = DateTime::createFromFormat($format, $date);
        return $d && $d->format($format) === $date;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $inputError = [];
        
        if ($filename = $input->getArgument('csv_filename')) {
            $ext = pathinfo($filename)['extension'];
            if ($ext != 'csv') {
                $inputError = ["csv_error" => "Please provide a csv file"];
            } else {
                $contentArray  = file($filename, FILE_IGNORE_NEW_LINES);
            }
        }

        if (array_key_exists('csv_error', $inputError)) {
            $responseArr = $inputError;

            $output->writeln(new JsonResponse($responseArr));
            return ;
        }

        $dateInput = $input->getArgument('expiry_date_value');

        if (strpos($dateInput, '/') !== false) {
            $parts =  explode('/', $dateInput);
        }

        if (strpos($dateInput, '.') !== false) {
            $parts = explode('.', $dateInput);
        }

        if (preg_match('/\s/', $dateInput)) {
            $parts = explode(' ', $dateInput);
        }

        if (strpos($dateInput, '-') === false) {
            $dateInput = implode('-', $parts);
        }

        $dateValid = $this->validateDate($dateInput);

        if ($dateValid != 1) {
            $output->writeln(new JsonResponse(["invalid_date" => "invalid date/format , fomat[yyyy-mm-dd]"]));
            return ;
        }

        $insertCmdService = $this->getApplication()->getKernel()->getContainer()->get('DbInsertCmd.Service');
        $resultArr = $insertCmdService->insertVouchersCmd($contentArray, $input->getArgument('operator_id_name'), $input->getArgument('expiry_date_value'));

        if (array_key_exists("id_not_found", $resultArr)) {
            $responseArr = [
                "message"=>"id not found"
            ];
            $output->writeln(new JsonResponse($responseArr));
            return ;
        }

        if (array_key_exists('DBALException', $resultArr)) {
            $responseArr = $resultArr;
            $output->writeln(new JsonResponse($responseArr));
            return ;
        }
        $output->writeln(new JsonResponse($resultArr));
    }
}
