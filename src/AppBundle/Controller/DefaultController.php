<?php

namespace AppBundle\Controller;

use AppBundle\EventListener;
use AppBundle\Entity\Operator;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\JsonResponse;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Component\HttpKernel\EventListener\ExceptionListener;
use Doctrine\DBAL\DBALException;

/**
 * DefaultController
 * DefaultController containing various actions
 */
class DefaultController extends Controller
{
    /**
     * @Route("/operator", name="get_all_operators")
     * @Method({"GET"})
     * function to get all operators id, name, network-id
     */
    public function operatorAction(Request $request)
    {
        
        $operRepo= $this->getDoctrine()->getRepository(Operator::class);
        $allOper = $operRepo->findAll();
       
        $ar = [];
       
        foreach ($allOper as $value) {
           array_push($ar, [
                'id'    => $value->getId(),
                'name'  => $value->getName(),
                'network-id'  => $value->getNetworkId()
            ]);
        }

        return new JsonResponse($ar);
    }

    /**
     * @Route("/operator", name="insert_new_operator")
     * @Method({"POST"})
     * function to insert new operators name and netId  
     */
    public function operatorInsAction(Request $request)
    {
        $params = [];
        $content = $request->getContent();
        $params = json_decode($content, true); // 2nd param to get as array
        $checkInputService = $this->container->get('InputValidation.Service');
        $isValidInput = $checkInputService->inputValidationOperator($params) ;
        
        if ($isValidInput) {
            $insertDbService = $this->container->get('DbInsertCmd.Service');
            $responseArray = $insertDbService->insertToDb($params);
            return new JsonResponse($responseArray);
        } else {
            $responseArray = [
                            'status' => 1,
                            'message'=> "invalid param"
                        ];
            return new JsonResponse($responseArray);
        }
    }

    /**
    * @Route("/get_voucher", name="get_voucher")
    * @Method({"POST"})
    * function to get vouchers of given amount, network , quantity 
    */
    public function getVoucherAction(Request $request)
    {
        $params = [];
        $content = $request->getContent();
        $params = json_decode($content, true); // 2nd param to get as array
        $checkInputService = $this->container->get('InputValidation.Service');
        $isValidInput = $checkInputService->inputValidationVoucher($params) ;
        
        if ($isValidInput) {
            $voucherRepo = $this->getDoctrine()->getManager()->getRepository('AppBundle:Voucher');
            $data = $voucherRepo->voucherPostData($params);
            if (!$data) {
                return new JsonResponse(["message" => "no record found"]);
            }
            return new JsonResponse($data);
        } else {
            $responseArray = [
                            'status' => 1,
                            'message'=> "invalid param"
                        ];
            return new JsonResponse($responseArray);
        }
    }

    /**
     * @Route("/operator/{id}", name="get_one_operator_details")
     * @Method({"GET"})
     * function to get a particular operator all details from operator table
     */
    public function fetchDataByIdAction($id)
    {
        $operRepo= $this->getDoctrine()->getRepository(Operator::class);
        $allOper = '';
        if (is_numeric($id)) {
            $allOper = $operRepo->find($id);
        }
        if (!$allOper) {
            $responseArray = [
                                'status' => 1,
                                'message'=> "id not exists in database"
                            ];
            return new JsonResponse($responseArray);
        }
        $ar = ['id' => $allOper->getId(),
              'name' => $allOper->getName(),
              'netId' => $allOper->getNetworkId(),
            ];

        return new JsonResponse($ar);
    }
}
