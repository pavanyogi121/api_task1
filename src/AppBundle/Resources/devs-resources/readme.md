install Postman Interceptor chrome extension and enable it in postman linux app, then visit this url

--------------------------------------------------------------------------------------
1.
GET http://local.api-task1.com/operator  or http://local.api-task1.com/app_dev.php/operator

[
    {
        "id": 1,
        "name": "airtel",
        "network-id": "1011"
    },
    {
        "id": 2,
        "name": "reliance",
        "network-id": "102"
    },
    ...
    {
        "id": 38,
        "name": "airtel4",
        "network-id": "125"
    }
]

http://local.api-task1.com/operator/1

{
    "id": 1,
    "name": "airtel",
    "netId": "1011"
}
---------------------------------------------------------------
2.
insert new operators name and netId
POST http://local.api-task1.com/operator
{
    "netId":"126",
    "name":"airtel5"
}

o/p:

{
    "status": "0",
    "data": {
        "id": 39,
        "name": "airtel5",
        "netId": "126"
    }
}
---------------------------------------------------------------
3.
CreateCustomeUsrCommand
class for custom user command to upload pin numbers to the database from csv file.
usage -   php bin/console  create_custome_usr "serial_numbers.csv" "1"  "2018-06-07"


    ->setName('create_custome_usr')
    ->setDescription('Creates a new user.')
    ->addArgument('csv_filename', InputArgument::REQUIRED, 'The filename of the csv file ')
    ->addArgument('operator_id_name', InputArgument::REQUIRED, 'The operator_id_name ')
    ->addArgument('expiry_date_value', InputArgument::REQUIRED, 'The expiry_date_value ')
    ->addOption('option', null, InputOption::VALUE_NONE, 'Option description')
;

---------------------------------------------------------------
4.
get vouchers of given amount, network , quantity
POST http://local.api-task1.com/get_voucher
{
"netId": "1",
"amount": "100",
"quantity": "1"
}

output:
[
    {
        "voucher_id": 27,
        "voucher_amt": "100",
        "operator_name": "airtel"
    }
]

-----------------------------------------------------------------




